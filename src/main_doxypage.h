/// \mainpage Umelá Inteligencia: Genetické programovanie \n Hľadanie pokladu
/// \image html chromozon.gif "Genetické programovanie"
/// \author Tomáš Kundis
/// \section s1 Hľadanie pokladu
///
/// Projekt č.: 3 \n
/// Originálny text zadania: <a href="http://www2.fiit.stuba.sk/~kapustik/poklad.html" target="_blank">http://www2.fiit.stuba.sk/~kapustik/poklad.html</a>
/// <ul>
/// <li>C++</li>
/// <li>Nokia QT v.: 4.8.0</li>
/// </ul>
///
/// <table><tr><td>
/// Umelá inteligencia \n
/// FIIT, STU v Bratislave \n
/// letný semester \n
/// 3. ročník \n
/// ak.r.: 2011/2012 \n
/// </table></td></tr>


