#ifndef MYEXCEPTIONS_HH
#define MYEXCEPTIONS_HH

#include <string>
#include <exception>

using namespace std;

/// Moje globalne vynimky, univerzalne pouzitie.

namespace MEX {

class myex_nullptr : public exception
{
public:
    const char *what() const throw() {
        return "myex_nullptr";
    }
};

class myex_alljwl : public exception
{
public:
    const char *what() const throw() {
        return "myex_nullptr";
    }
};

class myex_badparams : public exception
{
public:
    const char *what() const throw() {
        return "myex_badparams";
    }
};



extern MEX::myex_nullptr myex_nullptr_object;
extern MEX::myex_alljwl myex_alljwl_object;
extern MEX::myex_badparams myex_badparams_object;

}

#endif // MYEXCEPTIONS_HH
