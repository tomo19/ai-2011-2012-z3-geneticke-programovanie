#ifndef MPCU_HH
#define MPCU_HH

/// ladiace vypisy mikroprogramovej jednotky
//#define MCU_DEBUG

#include "HW.h"                     // MAX_MEMCELL
#include <bitset>
#include <exception>

using namespace std;

class VMachine;

// ====================================================================================
// namespace MCUnit_N
// ====================================================================================

/// MCUnit - menny priestor

namespace MCUnit_N {

/// Zoznam instrukcii

enum e_is {
    I_INC = 0x0,                        ///< inkrementacia hodnoty
    I_DEC = 0x1,                        ///< dekerementacia hodnoty
    I_JMP = 0x2,                        ///< nepodmieneny skok
    I_LIST = 0x3                        ///< ziadost CPU a vypis
};

// vynimky spracovavane rozhraniami MCUnit

/*
class mcuex_null : public exception {
public:
    const char *what() const throw() {
        return "mcuex_null";
    }
};
*/

class mcuex_noinst : public exception {
public:
    const char *what() const throw() {
        return "mcuex_noinstl";
    }
};

//extern mcuex_null mcuex_null_object;
extern mcuex_noinst mcuex_noinst_object;

}

// ====================================================================================
// class MCUnit_N
// ====================================================================================

/// Micro-programmable control unit (MPCU)

/// Mikroprogramovatelna jednotka, analogia hw architektury procesora - instrukcna sada.
/// Reprezentuje nezavisle instrukcnu sadu od okolia, ostatnych komponentov procesora,
/// ale aj napriek tomu je zviazana s procesorom a implementacia tejto triedy sa musi riadit pravidlami architektury procesora, pre ktory sa implementuje.
/// @warning \li Mometalne bez implementacia moznosti aby jedna instrukcia zaberala viac adries v pameti.
/// @warning \li Momentalne maximalny adresovaci priestor je taky, aby sa adresa vosla do jednej bunky. Pokym bude kapacita pamete mocnina dvojky, mame zarucene ze nedojde k generovaniu neplatnej adresy, preto tato instrukcia nevyhadzuje vynimku mcuex_badaddr.

class MCUnit
{
private:    
    VMachine *parent;                               ///< jednotka CPU ktorej patri mikroprogramovej jednotka
    int listStartAddress;                           ///< info. pre CPU, adresa prvej pametovej bunky vypisu, tuto informaciu si CPU vyziada volanim metody MCUnit::getListStAddr()

public:
    static const int minMemReq = 64;                ///< min. pozadovene mnozstvo pamete [B]
    static const int opCodeLen = 2;                 ///< dlzka operacneho kodu [bit]

public:
    MCUnit(VMachine * const parent);
    void exec(bitset<MAX_MEMCELL> * const instruction);
    inline int getListStAdd() const;
    string disasm(const bitset<MAX_MEMCELL> * const instruction, bool addInfo) const;
    bitset<MAX_MEMCELL> generate() const;
};

/// Vrati adresu v pameti odkial sa ma robit vypis.

/// Metoda musi byt implementovana, pouziva ju VMachine::notifyToDoList()
/// \return MCUnit::listStartAddress adresa v pameti, zaciatok vypisu
inline int MCUnit::getListStAdd() const
{
    return this->listStartAddress;
}

#endif // MPCU_HH
