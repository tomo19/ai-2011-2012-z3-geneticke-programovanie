#ifndef GAMEWORLD_HH
#define GAMEWORLD_HH

#include <list>
#include <QHash>

using namespace std;

class Position;

#define MIN_HEIGTH 3
#define MIN_WIDTH 3
#define MAX_HEIGTH 100
#define MAX_WIDTH 100

/// GameWorld - menny priestor

namespace GW {

enum e_square {
    SQ_EMPTY = '.',
    SQ_JEWEL = 'M',
    SQ_FINDER = 'F',
    SQ_FONJ = '@'
};

/// pohyb hladac pokladvo o jedno policko na mriezke
enum e_move {
    MOV_H,              ///< pohyb nahor
    MOV_D,              ///< pohyb nadol
    MOV_P,              ///< pohyb vpravo
    MOV_L              ///< pohyb vlavo
};
}

/// 2D plocha, svet hladava pokladov

class GameWorld
{
private:    
    int heigth;                                 ///< x-os, pocet riadkov, indexovanych <0, height-1>
    int width;                                  ///< y-os, pocet stlpcov, indexovanych <0, width-1>
    char **grid;                                ///< mriezka - dvojrozmerne pole
    bool cross;                                 ///< spravanie sa na hrane mriezky, ak hladac narazi na koniec mriezky @value true - pokracuje od prveho policka @value nieje mozne prejst cez hranicu mriezky
    int findJewels;                             ///< pocet najdenych pokladov    
    int steps;                                  ///< pocet urobenych krokov
    Position *startPos;
    Position *actualPos;
    Position *gridPos;                          ///< pozicia hladaca pokladov na vystupe mriezky, pre ladiace ucely
    list<Position> jewels;                      ///< zoznam pokladov, dat. struktura lin. zoznam tried Position
public:
    QHash<int, Position> jewelsHash;            ///< zoznam najdenych pokladov
private:
    inline bool isJewel() const;    
    inline bool checkRange(int x, int y) const;
    inline void delActPos();    

public:
    GameWorld(int heigth, int width, bool cross);
    ~GameWorld();
    bool setStartPos(const Position * const pos);
    bool addJewel(const Position * const pos);
    bool move(GW::e_move direction);                    /// \todo prepisat na inline
    int getJewelsCount() const;
    int getFindedJewels() const;
    int getSteps() const;
    void restart();
    void clear();
    void updateGridPos();                               /// \todo zistit ci sa neda prepisat na private
    void toString();
};


// =======================================================================================
// int GameWorld::getFindedJewels()
// =======================================================================================

inline int GameWorld::getFindedJewels() const
{
    return this->findJewels;
}


// =======================================================================================
// int GameWorld::getSteps()
// =======================================================================================

inline int GameWorld::getSteps() const
{
    return this->steps;
}


// =======================================================================================
// int GameWorld::getJewelCount()
// =======================================================================================

inline int GameWorld::getJewelsCount() const
{
    return this->jewels.size();
}

#endif // GAMEWORLD_HH
