#include "GA.hh"
#include "Program.hh"
#include "VMachine.hh"
#include "GameWorld.hh"
#include "Position.hh"
#include "MyExceptions.hh"
#include <cstdlib>
#include <iostream>
#include <bitset>

using namespace std;

int main(int argc, char *argv[])
{    
    argc = 1;
    argv[1] = NULL;

    bool res = 0;
    int itmp = 0;

    int mriezka_x = 6;
    int mriezka_y = 6;
    bool mriezka_hranica = false;
    int memSize = 64;

    srand(time(NULL));

    VMachine *vm = NULL;
    GameWorld *world = NULL;

    // WORLD setum
    cout << "GameWorld:: (pocet) x = " << endl;
    cin >> itmp;
    mriezka_x = itmp;
    cout << "GameWorld:: (pocet) y= " << endl;
    cin >> itmp;
    mriezka_y = itmp;

    try {
        world = new GameWorld(mriezka_x, mriezka_y, mriezka_hranica);
        vm = new VMachine(memSize, world);
    }
    catch (MEX::myex_nullptr &e) {
        cout << "main(): " << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    catch (exception &e) {
        cout << "main(): " <<  e.what() << endl;
        exit(EXIT_FAILURE);
    }

    // moj test
    /*
    world->addJewel(new Position(1, 0));
    world->addJewel(new Position(1, 1));
    world->addJewel(new Position(1, 2));
    world->addJewel(new Position(1, 3));
    world->addJewel(new Position(1, 4));
    world->addJewel(new Position(1, 5));
    */

    world->addJewel(new Position(1, 4));
    world->addJewel(new Position(2, 2));
    world->addJewel(new Position(3, 6));
    world->addJewel(new Position(4, 1));
    world->addJewel(new Position(5, 4));


    // VMachine setup

    cout << "VMachine:: Max. pocet instrukcii: " << endl;
    cin >> itmp;

    int maxInstructions = itmp;

    cout << "VMachine:: Opakovat vykonavanie? [0/1]: " << endl;
    cin >> itmp;

    VM::e_programBehaviour bhv;
    if (itmp == 0)
        bhv = VM::BHV_NOLOOP;
    else
        bhv = VM::BHV_LOOP;

    VM::e_fitnes fitnes;

    cout << "VMachine::fitnes:: aj vzdialenosti? [0/1]: " << endl;
    cin >> itmp;
    if (itmp == 1)
        fitnes = VM::FITNES_JWLSTEPS;
    else
        fitnes = VM::FITNES_JWL;

    vm->setExecutionBhv(bhv);
    vm->setMaxInstructions(maxInstructions);
    vm->setFitnesFunc(fitnes);

    // GA setup

    cout << "GA:: pocet chromozonov v populacii: " << endl;
    cin >> itmp;
    int chromozones = itmp;

    cout << "GA:: pocet epoch: " << endl;
    cin >> itmp;
    int maxGenerations = itmp;

    GA *ga = NULL;
    try {
        ga = new GA(chromozones, vm);
    } catch (exception &e) {
        cerr << "main():: " << e.what() << endl; exit(EXIT_FAILURE);
    }

    ga->generateFirstPopulation();

    try {
        ga->runEvolution(maxGenerations);
    } catch (...) {}

    cout << "================================================================" << endl;
    cout << "                        RESULT" << endl;
    cout << "================================================================" << endl;
    world->restart();
    world->toString();
    vm->loadProgram(&ga->bestChromozon);
    vm->runProgram();
    vm->movsLogToString();
    cout << endl << "Pocet najdenych pokladov: " << vm->getJewels() << endl;

    delete vm;
    delete ga;
    delete world;

    return EXIT_SUCCESS;
}
