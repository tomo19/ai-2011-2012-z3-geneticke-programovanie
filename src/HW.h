#ifndef HW_H
#define HW_H

#define MAX_OPCODE_LEN 5
#define MAX_MEM_BUS_LEN 11
#define MAX_MEMCELL (MAX_OPCODE_LEN + MAX_MEM_BUS_LEN)

#endif // HW_H
