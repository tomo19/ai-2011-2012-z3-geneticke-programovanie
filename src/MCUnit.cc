#include "MCUnit.hh"
#include "VMachine.hh"
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <bitset>
#include <sstream>

using namespace std;

// =====================================================================
// MCUnit::MCUnit(VMachine *parent)
// =====================================================================

/// Konstruktor

/// Nastavi ukazoval na rodicovsku CPU jednotku.
/// @throw mcuex_null exception, ak parent = NULL
MCUnit::MCUnit(VMachine *const parent)
{
    this->parent = NULL;

    if (parent == NULL) {
        cerr << "ERROR! MCUnit::MCUnit(): parent = NULL!" << endl;
        //throw MCUnit_N::mcuex_null_object;
        throw MEX::myex_nullptr_object;
    }

    this->parent = parent;
}

// =====================================================================
// void MCUnit::exec(bitset<MAX_MEMCELL> *instruction
// =====================================================================

/// Vykona jednu intsrukciu

/// Ak ma instrukcia viac bitov ako jedna bunka(adresa) pamete, v IR je prva adresa instrukcia, dalsie data z dalsich buniek si metoda MCUnit::exec() zabezpeci sama priamou komunikaciou s pametou.
/// @param *instruction pointer na instrukciu, spravny by to mal byt pointer na <b>IR (instruction register)</b>
/// Instrukcia vypisu - rerusenie procesora:
/// <table> <tr><td>
/// <ul>
/// <li>ak instrukcia LIST</li>
/// <ul>
/// <li>VMachine::listStartAddress = addr // extracten from instruction</li>
/// <li>VMachine::notifyToDoList // poziadanie nadradenej CPU jednotky o prerusenie vypisu</li>
/// <ul>
/// <li>zachyt vynimku VMachine::notifyToDoList()</li>
/// <ul>
/// <li>PC++</li>
/// <li>throw zachytena vynimka (MEX::myex_alljwl)</li>
/// </ul>
/// </ul>
/// </ul>
/// </ul>
/// </td></tr></table>
/// @warning \li Mometalne bez implementacia moznosti aby jedna instrukcia zaberala viac adries v pameti.
/// @warning \li meni hodnotu *instruction !!! mal by byt IR, v ziadnom pripade nie priamy pristup do pamete, register <b>IR</b> je pocas vykonavanie instrukcie plne v sprave MCUnit
/// @throw MEX::myex_alljwl ak vyhodena pri volani VMachine::notifyToDoList()
/// @retval nema, ale    upravuje registre (clenske premenne) triedy VMachine (*parent):
/// @li PC- adresa dalsej instrukcie

void MCUnit::exec(bitset<MAX_MEMCELL> * const instruction)
{
    int opcode = 0;                     // operacny kod
    int opcodeMask = 0;                 // maska operacneho kody, pocet bitov opcode uplne nalavo
    // pouziva sa na nulovanie nahodne nastavenych bitov v nadrozmernom bitovom retazci
    int mmask = 0;                      // podobne ako opcodeMask
    int addr = 0;                       // parsovana hodnota adresy z instrukcie

    unsigned int tmp = 0;               // docasna hotnota

#ifdef MCU_DEBUG
    stringstream stmp1;                 // pomocne vypisy - instruckia
    stringstream stmp2;                 // pomocne vypisy - pamet
#endif

    opcodeMask = pow(2, MCUnit::opCodeLen) - 1;         /// \todo konstanta vramci instancie - dat ako clen triedy - zrychli exec()
    mmask = pow(2, this->parent->maddrBusUsed) - 1;     /// \todo konstanta vramci instancie? - dat ako clen triedy - zrychli exec()

    // ---------------------------------------------------------------------------
    // operacny kod instrukcie - prve dva bity (MCUnit_N::opCodeLen)
    // ---------------------------------------------------------------------------

    // ! dochadza k modifikacii instrukcie
    opcode = ((*instruction) >> parent->maddrBusUsed).to_ulong();
    // osetrenie "rusivych" '1' v neplatnom rozsahu, ktory sa nepouziva vramci bunky pamete
    opcode = opcode & opcodeMask;

#ifdef MCU_DEBUG        
    stmp1 << "|" << *instruction << "| PC=" << this->parent->PC << " opcode: [" << hex << opcode << "] :: ";
#endif

    // ---------------------------------------------------------------------------
    // adresa parametre - kazda instrukcia ma jediny parameter - adresu
    // ---------------------------------------------------------------------------

    // ! dochadza k modifikacii instrukcie - od tejto doby uz iba adresa
    addr = ((*instruction).operator&=(mmask)).to_ulong();

#ifdef MCU_DEBUG
    stmp2 << "memory: [#" << addr << "]: (" << (*this->parent->mem)[addr].to_ulong() << ")";
#endif

    // -------------------------------------------------
    // vykonaj instrukciu
    // -------------------------------------------------
    switch (opcode) {

    case MCUnit_N::I_INC:
        // -------------------------------------------------
        // INC [memAddr]
        // task: zvys hodnotu v pameti na adrese memAddr o jedna
        // params: adresa v pameti
        // -------------------------------------------------

        tmp = (*this->parent->mem)[addr].to_ulong();
        tmp++;
        (*this->parent->mem)[addr] = tmp;

        this->parent->PC++;

#ifdef MCU_DEBUG
        stmp1 << "INC (" << dec << addr << ")" << " (0x" << hex << addr << ") (" << *instruction << ")  ";
        stmp2 << " [#" << addr << "_n]: (" << (*this->parent->mem)[addr].to_ulong() << ")";
        cout << stmp1.str() << stmp2.str() << endl;
#endif
        break;

    case MCUnit_N::I_DEC:
        // -------------------------------------------------
        // DEC [memAddr]
        // task: zniz hodnotu v pameti na adrese memAddr o jedna
        // params: adresa v pameti
        // -------------------------------------------------

        tmp = (*this->parent->mem)[addr].to_ulong();
        tmp--;
        (*this->parent->mem)[addr] = tmp;

        this->parent->PC++;

#ifdef MCU_DEBUG
        stmp1 << "DEC (" << dec << addr << ")" << " (0x" << hex << addr << ") (" << (instruction->operator &=(mmask)) << ")  ";
        stmp2 << " [#" << addr << "_n]: (" << (*this->parent->mem)[addr].to_ulong() << ")";
        cout << stmp1.str() << stmp2.str() << endl;
#endif
        break;

    case MCUnit_N::I_JMP:
        // -------------------------------------------------
        // JMP [memAddr]
        // task: skok na miesto v programe
        // params: adresa v pameti
        // -------------------------------------------------

        this->parent->PC = addr;

#ifdef MCU_DEBUG
        stmp1 << "JMP (" << dec << addr << ")" << " (0x" << hex << addr << ") (" << (instruction->operator &=(mmask)) << ") PC=" << this->parent->PC;
        cout << stmp1.str() << endl;
#endif
        break;

    case MCUnit_N::I_LIST:
        // -------------------------------------------------
        // LIST [memAddr]
        // task: vypis
        // note: vysle ziadost na jednotku cpu o vykonanie vypisu pamete od adresy memAddr
        // params: adresa v pameti
        // -------------------------------------------------
#ifdef MCU_DEBUG
        stmp1 << "LIST (" << dec << addr << ")" << " (0x" << hex << addr << ") (" << (instruction->operator &=(mmask)) << ")  ";
        //stmp2 << " [#" << addr << "_n]: (" << (*this->parent->mem)[addr].to_ulong() << ") PC=" << parent->PC;
        //cout << stmp1.str() << stmp2.str() << endl;
        cout << stmp1.str() << endl;
#endif

        this->listStartAddress = addr;

        try {
        this->parent->notifyToDoList();
    } catch (MEX::myex_alljwl &e) {            
            this->parent->PC++;
            throw e;
        }

        this->parent->PC++;

        break;

    default:
        // neplatna instrukcia
        // nepripustne, vyuzite vsetky permutacie bitov v opcode
        throw MCUnit_N::mcuex_noinst_object;
        break;
    }
}

// =====================================================================
// bitset<MAX_MEMCELL> MCUnit::disasm()
// =====================================================================

/// Zo strojoveho kodu vrati v textovej podobe zapis instrukcie.

/// @param *const - instrukcia
/// @param addInfo - zobrazi aj dodatocne info k instrucii, a to parameter v hexadecimalnom a binarnom tvare, napr.: INC (49) (0x31) (0000000000110001)
/// @returns textova reprezentacia instrukcie

string MCUnit::disasm(const bitset<MAX_MEMCELL> *const instruction, bool addInfo = true) const
{
    bitset<MAX_MEMCELL> tmp;
    stringstream s;

    string stmp;

    int opcode = 0;
    int opcodeMask = 0;
    int addr = 0;
    int mmask = 0;

    opcodeMask = pow(2, MCUnit::opCodeLen) - 1;       /// \todo konstanta vramci instancie - dat ako clen triedy - zrychli disasm()
    mmask = pow(2, this->parent->maddrBusUsed) - 1;     /// \todo konstanta vramci instancie? - dat ako clen triedy - zrychli disasm()

    tmp = *instruction;

    opcode = (tmp >> parent->maddrBusUsed).to_ulong();
    opcode = opcode & opcodeMask;

    addr = (tmp.operator &=(mmask)).to_ulong();

    switch (opcode) {
    case MCUnit_N::I_INC:
        s << "INC ";
        break;
    case MCUnit_N::I_DEC:
        s << "DEC ";
        break;
    case MCUnit_N::I_JMP:
        s << "JMP ";
        break;
    case MCUnit_N::I_LIST:
        s << "LIST ";
        break;
    }

    s << "[" << addr << "] ";

    // zobrazi aj dodatocne informacie
    if (addInfo == true)  {
        if (addr < 0xA)
            s << "(0x0" << hex << addr << ") (" << tmp << ")";
        else
            s << "(0x" << hex << addr << ") (" << tmp << ")";
    }

    stmp = s.str();

    return stmp;
}

// =====================================================================
// bitset<MAX_MEMCELL> MCUnit::generate()
// =====================================================================

/// Vygeneruje nahodnu instrukciu.

/// Vytvori nahodne generovanu instrukciu. Maximalna dlzka instrukcie zaberie jednu pametovu bunku.
/// \returns Nahodne vygenerovana instrukcia v celej jej dlzke.
///
bitset<MAX_MEMCELL> MCUnit::generate() const
{
    bitset<MAX_MEMCELL> tmp;

    int opcode = 0;
    int addr = 0;

    // generuj opcode a adresu    
    opcode = rand() % (int) pow(2, this->opCodeLen);
    addr = rand() % this->parent->getMemSize();

    tmp = opcode;
    tmp.operator <<=(this->parent->maddrBusUsed);
    tmp.operator|=(addr);

    return tmp;
}
