#include "Program.hh"
#include <limits>

/// Konstruktor

/// @note implicitne - dlzka programu 8 B
/// @note implicitne - fitnes hodnota = nekonecno (numeric_limits<unsigned int>::max()) v <limits>
Program::Program()
{
    this->mcode.resize(8);
    this->fitnes = numeric_limits<double>::max();
}
