#ifndef VMACHINE_HH
#define VMACHINE_HH

#include "HW.h"
#include "MCUnit.hh"
#include "MyExceptions.hh"
#include "GameWorld.hh"
#include <cmath>
#include <iostream>
#include <vector>
#include <bitset>

using namespace std;

#define MAX_INSTRUCTIONS_DEF 500

class Program;
class GameWorld;

// ========================================================================================
// namespace VM
// ========================================================================================

/// VMachine - menny priestor

namespace VM {

/// Spravanie sa vykonavania programu po dosiahnuti konca pamete, tj. poslednej instrucie.
enum e_programBehaviour {
    BHV_LOOP,               ///< vykonavanie programu bude poracovat prvou instrukcou na VMachine::mem[0]
    BHV_NOLOOP              ///< vykonavanie programu sa ukonci
};

/// Vyber funkcie na ohodnotenie fitnes programu.

/// Nastaveuje sa: VMachine::setFitnes(e_fitnes func)
///
enum e_fitnes {
    FITNES_JWL,             ///< pocet pokladov, IMPLICITNE
    FITNES_JWLSTEPS         ///< pocet pokladov pri co najkratsej vzdialenosti
};

}

// ========================================================================================
// class VMachine
// ========================================================================================

/// Virtualny stroj

/// Virtualny stroj pre vykonanie softverovo geneticky naprogramovaneho strojoveho kodu hladaca pokladov.
/// @note @li implicitna fitnes funkcia nastavena konstruktorom - VMachine::fitnes1()
/// @note @li implicitna funkcia na interpretacie MEM[addr] -> {D, H, P, L} nastavena konstruktorom - VMachine::intrMove()
/// @todo moznost dynamicky menit mikroprogramovu jednotku, alebo inicializacia v konstruktore
class VMachine
{
// privatne clenske premenne
private:
    int IC;                                           ///< IC register, Instruction Counter, pocet vykonanych instrukcii
    unsigned int memSize;                             ///< velkost pamete (pocet buniek)
    MCUnit *mcu;                                      ///< MCU jednotka, tj. instrukcna sada programu
    GameWorld *world;                                 ///< 2D svet, na ktorom sa simuluje pohyb a pocita pocet najdenych pokladov, pocas vykonavania programu
    int maxInstructions;                              ///< maximalny povoleny pocet vykonanych instrukcii, po ktorych sa vykonavanie programu bezpodmienecne zastavy, implicitne nastavene na MAX_INSTRUCTIONS_DEF, tj. 500 instrukcii
    VM::e_programBehaviour progBhv;                   ///< vykonavanie programu po dosiahnuti poslednej instrukcie, implicitne VM::BHV_NOLOOP
    string movsLog;        
    Program *actualProg;                                ///< aktualne nacitany program pomocou VMachine::loadProgram()
    bool F_LIST;                                        ///< indikuje, ci bola aspon raz pocas vykonavania programu zavolana instrukcia vypisu

    GW::e_move (VMachine::*intm)(int addr);             ///< pointer na zvolenu funkciu interpretacie MEM[x] -> {D, H, P, L}
    double (VMachine::*computeFitnes)() const;          ///< pointer na zvolenu funkciu fitnes

// privatne metody
private:
    inline void runLOOP();
    inline void runNOLOOP();
    void doList();
    void doListExplicit();
    GW::e_move intrMove(int addr);
    double fitnes1() const;
    double fitnes2() const;

// public clenske premenne
public:
    bitset<MAX_MEMCELL> IR;                         ///< IR register, Instruction Register, prave vykonavana instrukcia
    unsigned int PC;                                ///< PC register, Program Counter, adresa nasledujucej instrukcie
    vector<bitset<MAX_MEMCELL> > *mem;              ///< pamet virtualneho stroja
    int maddrBusUsed;                               ///< sirka adresnej zbernice

// public metody
public:    
    VMachine(int memSize, GameWorld *world);
    ~VMachine();
    bool loadProgram(const Program * const prog);
    void runProgram();
    void notifyToDoList();                          /// \todo logickost: vyuzit vlastnosti C++, dat do private a nastavit frienda MCUnit
    void setFitnesFunc(VM::e_fitnes func);
    void setMaxInstructions(int count);    
    void setExecutionBhv(VM::e_programBehaviour behaviour);
    int getMemSize() const;
    int getCellSize() const;
    int getJewels() const;                                /// \todo nepotrebne, iba informativne funkcie, pouzivat opatrne - zasadne inhned po volani VMachine::runProgram(), inak nedefinovana hodnota
    int getSteps() const;
    void movsLogToString() const;
    bool generate(bitset<MAX_MEMCELL> *instruction) const;
};

// ===============================================================================
//                          PUBLIC INLINE METHODS
// ===============================================================================

// ===============================================================================
// VMachine::notifyToDoList()
// ===============================================================================

/// Handler notifikacie od MCUnit triedy, CPU jednotka zavola univerzalnu instrukciu vypisu.

/// Zavola ta metodu VMachine::doList(), ta zavola <b>registrovanu metodu na vypis</b>. Tychto metod moze byt viacero.
/// @note @li Nastavy flag VMachine::F_LIST na hodnotu true
/// @note @li Momentalne je implementovana iba jedna metoda vypisu, preto chyba funkcia fegistracie metody a enumeracny zoznam tychto metod.
/// \todo aspon dve metody vypisu
/// \todo ich vyber a registracia

inline void VMachine::notifyToDoList()
{
    this->F_LIST = true;

    try {
        this->doList();
    } catch (MEX::myex_alljwl &e) {        
        throw e;
    }
}


// ===============================================================================
// VMachine::getMemSize()
// ===============================================================================
inline int VMachine::getMemSize() const
{
    return this->memSize;
}


// ===============================================================================
// inline int VMachine::getCellSize() const;
// ===============================================================================
inline int VMachine::getCellSize() const
{
    return this->maddrBusUsed + this->mcu->opCodeLen;
}


// ===============================================================================
// int VMachine::getJewels()
// ===============================================================================
inline int VMachine::getJewels() const
{
    return this->world->getFindedJewels();
}

// ===============================================================================
// int VMachine::getSteps()
// ===============================================================================
inline int VMachine::getSteps() const
{
    return this->world->getSteps();
}

// ===============================================================================
//                          PRIVATE INLINE METHODS
// ===============================================================================

// ===============================================================================
// VMachine::runNOLOOP()
// ===============================================================================

/// Vykonavanie programu

/// Vykona nacitany program. Po dosiahnuti poslednej instrukcie alebo maximalneho povoleneho poctu instrukcii VMachine::maxInstructions sa vykonavanie zastavy
/// @li ak PC obsahuje adresu do neznamej pamete, vykonavanie sa ukonci.
///
/// Instrukcny cyklus:
/// <ul>
/// <li>pokial VMachine::IC < VMachine::maxInstructions</li>
/// <ul>
/// <li>IR = memory[PC]</li>
/// <li>mcu.exec(IT)</li>
/// <ul>
/// <li>catch exception: MEX::myex_alljwl</li>
/// <ul><li>then throw: MEX::myex_alljwl</li></ul>
/// </ul>
/// </ul>
/// <li>IC++</li>
/// </ul>
///
/// @throw MEX::myex_alljwl ak vyniku vyhodi MCUnit::exec()
inline void VMachine::runNOLOOP()
{
    // ------------------------------
    // instrukcny cyklus
    // ------------------------------
    // pre max. pocet vykonanych instruckii....
    while (this->IC < this->maxInstructions) {

        // ak adresa do neexistujucej pamete - ukonci vykonavanie programu
        if (this->PC >= this->mem->size()) {
            cerr << "MSG: VMachine::runNOLOOP(): program execution end because instruction on last memory cell" << endl;
            return;
        }

        this->IR = (*this->mem)[PC];

        try {
            this->mcu->exec(&this->IR);
        } catch (MEX::myex_alljwl &e) {            
            throw e;
        }

        this->IC++;
    }
}

// ===============================================================================
// VMachine::runLOOP()
// ===============================================================================

/// Vykonavanie programu, cyklicky opakujuce sa vykonavanie programu

/// Vykona nacitany program. Po dosiahntu poslednej instrukcie sa program zacina vykonavat od adresy 0x00.
/// Vykonavanie programu sa bezpodmienecne zastavy po dosiahnuti maximalneho povoleneho poctu instrukcii VMachine::maxInstructions sa vykonavanie zastavy.
///
/// Instrukcny cyklus:
/// <ul>
/// <li>pokial VMachine::IC < VMachine::maxInstructions</li>
/// <ul>
/// <li>if PC >= VMachine::mem.size()</li>
/// <ul><li>PC = 0</li></ul>
/// <li>IR = memory[PC]</li>
/// <li>mcu.exec(IT)</li>
/// <ul>
/// <li>catch exception: MEX::myex_alljwl</li>
/// <ul><li>then throw: MEX::myex_alljwl</li></ul>
/// </ul>
/// </ul>
/// <li>IC++</li>
/// </ul>
///
/// @throw MEX::myex_alljwl ak vyniku vyhodi MCUnit::exec()


inline void VMachine::runLOOP()
{
    // ------------------------------
    // instrukcny cyklus
    // ------------------------------
    while (this->IC < this->maxInstructions) {
        if (this->PC >= this->mem->size())
            this->PC = 0;

        this->IR = (*this->mem)[PC];

        try {
            this->mcu->exec(&this->IR);
        } catch (MEX::myex_alljwl &e) {
            throw e;
        }

        this->IC++;
    }
}


// ===============================================================================
// VMachine::doList()
// ===============================================================================

/// Instrukcia vypisu

/// Vypis {H, D, P, L} instrukcii pre hladaca poklada v GameWorld 2D svete.
/// Vypise obsah pamete od zadanej adresy po koniec pamete.
///
/// Obsah vypisu pripoji k VMachine::movsLog
/// @note @li adresa zaciatku vypisu sa ziastuje zo zaregistrovanej mkrijednotky MCUnit::getListStAdd()
/// @note @li az tato metoda prevadza interetaciu bunky na ozajstny textovy znak (z GE::e_move)
/// @warning pouziva MCU, ci je MCU registrovana nekontrolujeme kvoli rychlosti kodu => zarucit konzistenost na vyssich vrstvach
/// @throw MEX::myex_alljwl ak su najdeme vsetky poklady
inline void VMachine::doList()
{
    int addr = this->mcu->getListStAdd();       // zaciatocna adresa vypisu
    GW::e_move m;

    for (unsigned int i = 1; i <= 1; i++) {
    //for (unsigned int i = addr; i < this->memSize; i++) {
        m = (this->*intm)(i);
        if (m == GW::MOV_H)
            this->movsLog.append("H");
        if (m == GW::MOV_D)
            this->movsLog.append("D");
        if (m == GW::MOV_P)
            this->movsLog.append("P");
        if (m == GW::MOV_L)
            this->movsLog.append("L");

        this->world->move(m);

        // nasli sme poklad?
        if (this->world->getFindedJewels() == this->world->getJewelsCount()) {
            cerr << "MSG: VMachine::doList(): all jewel finded" << endl;
            throw MEX::myex_alljwl_object;
        }
    }
}

// ===============================================================================
// VMachine::doListExplicit()
// ===============================================================================

/// Instrukcia vypisu - explicitne volanie

/// To iste ako metoda VMachine::doVypis(), ale zaciatok vypisu je dany explicitne, od adresy 0.
///
/// Pouziva VMachien::runProgram(), ak sa pocas vykonavania programu nevyskytla ziadna instrukcia vypisu, a VMachine::F_LIST == false.
/// @warning pouziva MCU, nekontrolujeme kvoli rychlosti kodu => zarucit konzistenost na vyssich vrstvach
/// @throw MEX::myex_alljwl ak su najdeme vsetky poklady

inline void VMachine::doListExplicit()
{
    //int addr = this->mcu->getListStAdd();       // zaciatocna adresa vypisu
    int addr = 0;
    GW::e_move m;

    for (unsigned int i = addr; i < this->memSize; i++) {
        m = (this->*intm)(i);
        if (m == GW::MOV_H)
            this->movsLog.append("H");
        if (m == GW::MOV_D)
            this->movsLog.append("D");
        if (m == GW::MOV_P)
            this->movsLog.append("P");
        if (m == GW::MOV_L)
            this->movsLog.append("L");

        this->world->move(m);

        // nasli sme poklad?
        if (this->world->getFindedJewels() == this->world->getJewelsCount()) {
            cerr << "MSG: VMachine::doList(): all jewel finded" << endl;
            throw MEX::myex_alljwl_object;
        }
    }
}


// ===============================================================================
// VMachine::intrMove()
// ===============================================================================

/// @todo spravit lepsiu logiku, nie iba "napevne" ale dynamicky od rozsahu bunky
/// @warning Vyzaduje MCU, nekontroluje sa
///
inline GW::e_move VMachine::intrMove(int addr)
{
    int mask = 0;                  // maska instrukcie
    unsigned int cellLen = 0;               // velkost bunky pamete
    int count = 0;                 // pocet jednotiek

    bitset<MAX_MEMCELL> tmp = (*this->mem)[addr];
    bitset<MAX_MEMCELL> tmp2;

    // vynuluj vsetky ostatne nepouzivane bity
    cellLen = this->mcu->opCodeLen + this->maddrBusUsed;
    mask = pow(2, cellLen) - 1;
    tmp.operator &=(mask);
    tmp2 = tmp;

    // spocitaj vsetky jednotky
    tmp2 = tmp;
    tmp2.operator &=(1);
    if (tmp2 == 1)
        count++;
    for (unsigned int i = 1; i < cellLen; i++) {
        tmp = tmp >> 1;
        tmp2 = tmp;
        tmp2.operator &=(1);
        if (tmp2 == 1)
            count++;
    }

    // interpretuj pocet jednotiek ako instrukciu {H, D, L, P} pre pohyb na mape
    if (count <= 2)
        return GW::MOV_H;
    if (count <= 4)
        return GW::MOV_D;
    if (count <= 6)
        return GW::MOV_P;

    return GW::MOV_L;
}

// ===============================================================================
// double VMachine::fitnes1()
// FITNES: pocet pokladov
// ===============================================================================

/// Fitnes - pocet pokladov

/// @return pocet najdenych pokladov
///
inline double VMachine::fitnes1() const
{    
    return this->world->getFindedJewels();
}

// ===============================================================================
// double VMachine::fitnes2()
// FITNES: pocet pokladov + vzdialenost (co najmenej prejdenych policok)
// ===============================================================================

/// Fitnes - pocet najdenych pokladov a co najmensi pocet krokov

/// @note fitnes = pocet pokladov - pocet krokov / 1000
/// @return fitnes vypocitana fitnes hodnota
inline double VMachine::fitnes2() const
{
    double x1 = (double) this->world->getFindedJewels();
    double x2 = (double) this->world->getSteps() / (double) 1000.0;

    double fitnes = x1 - x2;

    return fitnes;
}

#endif // VMACHINE_HH

