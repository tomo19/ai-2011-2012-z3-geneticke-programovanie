#ifndef POSITION_HH
#define POSITION_HH

/// Pozicia, suradnice v GameWorld.

class Position
{
public:
    int x;
    int y;

public:    
    Position(int x, int y);    
    int hash() const;
    static int hash(int x, int y);
};

#endif // POSITION_HH
