#include "Position.hh"
#include "GameWorld.hh"
#include <iostream>

#include <cstdlib>

using namespace std;

Position::Position(int x, int y)
{    
    this->x = x;
    this->y = y;
}

/// @warning prisposobene pre max. mriezku 100x200
///
int Position::hash() const
{
    return ((this->x*10000) + (this->y*100));
}

/// @warning prisposobene pre max. mriezku 100x200
///
int Position::hash(int x, int y)
{
    return (x*10000 + y*100);
}
