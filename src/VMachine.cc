#include "VMachine.hh"
#include "Program.hh"
#include "MCUnit.hh"
#include "GameWorld.hh"
#include "Position.hh"
#include "MyExceptions.hh"
#include <cmath>
#include <iostream>
#include <vector>
#include <bitset>
#include <limits>

using namespace std;

// ===============================================================================
// VMAchine::VMachine(int memSize)
// ===============================================================================

/// Konstruktor

/// @li restart GameWorld
/// @li nastavenie velkosti pamete (pocet buniek)
/// @li nastavenie sirky adresovej zbernice
/// @li vytvorenie pamete: this->mem = new vector<bitset<MAX_MEMCELL> >
/// @li inicializacia mikroprogramovej jednotky MCUnit
///
/// @param memsize - pocet buniek pamete
/// @param *world - inicializovana 2D mriezka, vytvorena mimo instancie tejto triedy
/// @note @li velkost pamete v zmysle poctu buniek v RAM, nie pocet MB
/// @note @li implicitna fitnes funkcia nastavena konstruktorom - fitnes 1
/// @note @li implicitna funkcia na interpretaciu MEM[addr] -> {D, H, P, L} nastavena konstruktorom - intrMove(addr)
/// @throw MEX::myex_nullptr
/// @throw std::bad_alloc
/// @todo  GameWorld - prava agregacia (plna sipka), pri paralelnej verzii algoritmu nesmie byt GameWorld zdielana trieda, kazdy VM spusta iny program
VMachine::VMachine(int memSize, GameWorld *world)
{
    this->IC = 0;
    this->mcu = NULL;

    this->IR.reset();
    this->PC = 0;

    this->mem = NULL;
    this->maddrBusUsed = 0;
    this->memSize = 0;

    this->world = NULL;

    this->movsLog.clear();

    this->actualProg = NULL;
    this->F_LIST = false;
    this->maxInstructions = MAX_INSTRUCTIONS_DEF;
    this->progBhv = VM::BHV_NOLOOP;

    // implicitne hodnoty callback funkcii

    this->intm = &VMachine::intrMove;
    this->computeFitnes = &VMachine::fitnes1;

    // ---------------------------------------------
    // nastav GameWorld
    // ---------------------------------------------

    if (world == NULL) {
        cerr << "ERROR! VMachine::VMachine(): world = NULL" << endl;
        throw MEX::myex_nullptr_object;
    }

    this->world = world;
    this->world->restart();

    // ---------------------------------------------
    // vytvor pamet
    // ---------------------------------------------
    /// \todo kontrola velkosti pamete: 64, 128, ... aby velkost bola mocnina dvojky
    try {
        this->mem = new vector<bitset<MAX_MEMCELL> >;
    } catch (bad_alloc &e) {
        cerr << "ERROR! VMachine::VMachine(): this->mem alloc failed" << endl;
        this->~VMachine();
        throw e;
    }
    this->mem->resize(memSize);
    this->maddrBusUsed = log2(memSize);
    this->memSize = memSize;

    // vynuluj pamet
    vector<bitset<MAX_MEMCELL> >::iterator it = this->mem->begin();
    while (it != this->mem->end()) {
        (*it).reset();
        it++;
    }

    cerr << "VMachine::VMachine(): mem size = [" << memSize << "]; maddrBusUsed = [" << this->maddrBusUsed << "]" << endl;

    // ---------------------------------------------
    // inicializuj mikroprogramovu jednotku
    // ---------------------------------------------
    try {
        this->mcu = new MCUnit(this);
    } catch (bad_alloc &e) {
        cerr << "ERROR! VMachine::VMachine(): this->mcu alloc failed" << endl;
        this->~VMachine();
        throw e;
    } catch (MEX::myex_nullptr &e) {
    //} catch (MCUnit_N::mcuex_null &e) {
        this->~VMachine();
        throw e;
    }

    return;
}

// ===============================================================================
// VMachine::~VMachine()
// ===============================================================================

/// Destruktor

/// @li uvolni pamet
/// @li uvolni MCU
VMachine::~VMachine()
{
    if (this->mem != NULL) {
        delete this->mem;
        this->mem = NULL;
    }

    if (this->mcu != NULL) {
        delete this->mcu;
        this->mcu = NULL;
    }
}


// ===============================================================================
// VMachine::loadProgram()
// ===============================================================================

/// Nacita program do pamete virtualneho stroja

/// @li skopiruj program do pamete
/// @li nastav pointer VMachine::actualProg na povodny program, tam sa po ukonceni programu zapise fitnes programu
///
/// @retval true ak vsetko v poriadku
/// @retval false ak chya pocas nacitavania (napr. nedostatok pamete)
bool VMachine::loadProgram(const Program *const prog)
{
    unsigned int progSize = 0;

    // NULL pointer na program
    if (prog == NULL) {
        cerr << "ERROR! VMachine::loadProgram(): prog = NULL" << endl;
        return false;
    }

    // program zabera viac miesta ako je pamete (pocet cells)
    if (prog->mcode.size() > this->memSize) {
        cerr << "ERROR! VMachine::loadProgram(): program->mcode.size()=" << prog->mcode.size() << "; this->memSize=" << this->memSize << "; Program is too BIG!" << endl;
        return false;
    }

    // velkost programu je 0
    if (prog->mcode.size() == 0) {
        cerr << "ERROR! VMachine::loadProgram(): prog->mcode.size = 0; Program is zero-lenght!" << endl;
        return false;
    }

    // skpiruj program do pamete
    progSize = prog->mcode.size();
    for (unsigned int i = 0; i < progSize; i++) {
        (*this->mem)[i] = prog->mcode[i];
    }

    // nastav pointer na aktualny program - tam sa zapise fitnes, ked sa skonci vykonavanie kopie
    this->actualProg = (Program *) prog;

    return true;
}


// ===============================================================================
// VMachine::runProgram()
// ===============================================================================

/// Vykona program, nacitany s VMachine::loadProgram()

/// Dva typy vykonavania programu:
/// @li standartny, ak VMachine::progBhv = VM::BHV_NOLOOP
/// @li cyklicky, ak VMachine::progBhv = VM::BHV_LOOP
///
/// Volba typu vykonavania programu sa nastavuje volanim metody setExecutionBhv(VM::e_programBehaviour behaviour).
///
/// <ul>
/// <li> PC = 0 </li>
/// <li> IC = 0 </li>
/// <li> F_LIST = false </li>
/// <li> vycisti VMachine::movsLog </li>
/// <li> GameWorld restart </li>
///
/// <li> VMachine::runLOOP(), or VMachine::runNOLOOP()</li>
/// <li> if (F_LIST == false)</li>
/// <ul> <li>VMachine::doListExplicit()</li></ul>
/// <li>zachyt vynimku MEX:myex_alljwl</li>
/// <ul><li>vypocitaj fitnes</li></ul>
/// <li> vypocitaj fitnes</li>
/// </ul>
///
/// @warning zavislot - pozaduje aby bol nacitany program => (this->actualProg != NULL)
/// @throw MEX::myex_alljwl ak ju vyhodina jedna z run-subroutines

void VMachine::runProgram()
{        
    this->PC = 0;
    this->IC = 0;
    this->F_LIST = false;
    this->movsLog.clear();
    this->world->restart();

    // instrukcne cykly, vyber - dva typy

    try {

        // po vykonani poslednej instrukcie sa vykonavanie zastavy
        if (this->progBhv == VM::BHV_NOLOOP)
            this->runNOLOOP();

        // po vykonani poslednej instrukcie, vykonavanie zacina znovu od adresy 0x00
        if (this->progBhv == VM::BHV_LOOP)
            this->runLOOP();

        // ak nebola volana instruckcia vypisu - explicitny vypis na konci, od adresy 0
        if (this->F_LIST == false) {
            cerr << "WARNING! VMachine::runProgram(): F_LIST=false; Executing LIST instruction explicitly" << endl;
            this->doListExplicit();
        }

        // zachytenie vynimky - VSETKY POKLADY SA NASLI - nepokracujeme dalej
        // vyhodit vynimku - informovat vyssiu instanciu
    } catch(MEX::myex_alljwl &e) {
        this->actualProg->fitnes = (this->*computeFitnes)();
        throw e;    // vyhodenie vynimky ako informacia pre geneticky algoritmus
    }

    // compute fitnes, save to Program fitnes
    this->actualProg->fitnes = (this->*computeFitnes)();
}


// ===============================================================================
// void VMachine::setFitnesFunc(VM::e_fintes func)
// ===============================================================================
void VMachine::setFitnesFunc(VM::e_fitnes func)
{
    if (func == VM::FITNES_JWL)
        this->computeFitnes = &VMachine::fitnes1;

    if (func == VM::FITNES_JWLSTEPS)
        this->computeFitnes = &VMachine::fitnes2;
}



// ===============================================================================
// VMachine::setMaxInstructions
// ===============================================================================

/// Maximalny pocet instrukcii

/// Maximalny pocet vykonanych instrukcii, po ktorych sa program bezpodmienecne zastavi.
///
void VMachine::setMaxInstructions(int count)
{
    this->maxInstructions = count;
}

// ===============================================================================
// void VMachine::setExecutionBhv(VM::e_programBehaviour behaviour);
// ===============================================================================

void VMachine::setExecutionBhv(VM::e_programBehaviour behaviour)
{
    this->progBhv = behaviour;
}


// ===============================================================================
// void VMachine::movsLogToString() const
// ===============================================================================

void VMachine::movsLogToString() const
{
    for (unsigned int i = 0; i < this->movsLog.size(); i++) {
        cout << this->movsLog[i] << " ";
    }
    cout << endl;
}

// ===============================================================================
// bool VMachine::generate(bitset<MAX_MEMCELL> *instruction) const
// ===============================================================================

/// @warning pocita sa s tym, ze kazda mikroprogramova jednotka ma metodu na generovanie instrukcie
bool VMachine::generate(bitset<MAX_MEMCELL> *instruction) const
{
    *instruction = this->mcu->generate();

    return true;
}
