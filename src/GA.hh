#ifndef GA_HH
#define GA_HH

#include "Program.hh"
#include "VMachine.hh"
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

class VMachine;

#define MIN_PSIZE 20                    ///< minimalna velkost populacie

/// Geneticky algoritmus

/// GA nad populaciou programov triedy Program beziacich pod virtualnym strojom VMachine.
///
/// @note implicitne springsSize = 0.3, vytvorene konstruktorom

class GA
{
public:
    vector<Program> p1;
    vector<Program> p2;
    int t;                                       ///< aktualna populacia
    int tMax;                                    ///< maximalny pocet populacii
    int popSize;                                 ///< velkost populacie, pocet jedincov (chromozonov) v populacii
    int springsSize;                             ///< pocet novych potomkov do novej generacie

    double generationAvgFitnes;                  ///< priemerna fitnes jednej generacie
    Program bestChromozon;                       ///< jedinec s prieznou a po skonceni algoritmu najlepsou fitnes hodnotou

    VMachine *vm;

    void (GA::*evolution)(const vector<Program> * const p, vector<Program> * const p_new);

    //private:
public:
    void generateFirstPopulation();
    void newPopulationAlgorithm1(const vector<Program> *const p, vector<Program> *const p_new);
    void checkAndSetBest(const Program * const p);
    void mutation(Program *p);
    /// @todo prerobit elegantnejsie
    void krizenie(const vector<Program> * const populacia, vector<Program> *const nova_populacia, unsigned int i);


public:
    GA(int populationSize, VMachine *virtualMachine);
    ~GA();
    void runEvolution(int generations);
};

// ==================================================================================
//                              PRIVATE INLINE METODY
// ==================================================================================

// ===============================================================================
// inline void GA::checkAndSetBest(const Program * const p)
// ===============================================================================

/// Kontrola, je chromozon *p je (doposial) najlepsi chromozon v populacii

/// @param *p program - chromozon
/// @warning predpoklad: *p != NULL, nekotroluje sa to (kvoli efektivite programu)

inline void GA::checkAndSetBest(const Program * const p)
{
    // ak je jedinec nekonecno: prvy jedinec = najlepsi jedinec
    if (this->bestChromozon.fitnes == numeric_limits<double>::max()) {
        this->bestChromozon = *p;
        return;
    }

    // nasiel sa doposial lepsi jedinec

    // cout << "p: " << p->fitnes << "; best: " << this->bestChromozon.fitnes << endl;
    if (p->fitnes > this->bestChromozon.fitnes) {
        this->bestChromozon = *p;
    }
}


// ===============================================================================
// void GA::mutation(vector<Program> *p)
// ===============================================================================

/// Mutacia

/// @li vyber nahodneho chromozonu v programe
/// @li vo vybranom chromozone, vyber nahodneho genu, nasledne jeho negacia
///
/// @param p program, v ktorom ma nastat mutacia
/// @warning predpoklad: p != NULL, neosetruje sa! (kvoli efektivite)
///
inline void GA::mutation(Program *p)
{
    int cell = rand() % p->mcode.size();                              // vyberieme nahodny clen z populacie
    int position = rand() % this->vm->getCellSize();                  // vyber genu, ktory sa zmutuje

    // nahodne vybraty bit v danej bunke znegujeme
    (bitset<MAX_MEMCELL>) (p->mcode[cell])[position].flip();
}


// ===============================================================================
// inline void GA::krizenie(Program *p)
// ===============================================================================

/// Dvojbodove krizenie

/// Krizenie dvoch jedincov, krizenim vzniknu dvaja novy potomkovia.
/// @param *populacia - populacia jedincov, z ktorej sa budu vyberat kandidati do turnaja
/// @param *nova_populacia - pointer na populacia, do ktorej sa budu vkladat novy jedincy
/// @param i - pozicia v novej populacii, od ktorej sa zacnu kopirovat nove chromozony
/// @warning zapise dva cleny do novej populacia - osetrit krizenie v pripade, ak ostava uz iba jeden clen
/// @todo prerobit: tak, aby parametrom boli pointre do populacia, kde sa zapisu nove programy
inline void GA::krizenie(const vector<Program> * const populacia, vector<Program> *const nova_populacia, unsigned int pos)
{
    int hranica = 0;

    // nahodny vyber dvoch chromozonov z populacie
    int prg1 = rand() % populacia->size();                              // vyberieme nahodny clen z populacie
    int prg2 = rand() % populacia->size();                              // vyberieme nahodny clen z populacie

    // vyber hranicu v kratsom chromozene
    if ((*populacia)[prg1].mcode.size() < (*populacia)[prg2].mcode.size())
        hranica = rand() % (*populacia)[prg1].mcode.size();
    else
        hranica = rand() % (*populacia)[prg2].mcode.size();

    Program newP1;
    // dlzka noveho chromozonu1 = pocet genov po hranicu + pocet zvysnych genov z chromoznu2
    newP1.mcode.resize(hranica + ((*populacia)[prg2].mcode.size() - hranica));
    Program newP2;
    // dlzka noveho chromozonu1 = pocet genov po hranicu + pocet zvysnych genov z chromoznu1
    newP2.mcode.resize(hranica + ((*populacia)[prg1].mcode.size() - hranica));

    // vnik noveho chromozonu1
    for (int i = 0; i <= hranica; i++) {
        newP1.mcode[i] = (*populacia)[prg1].mcode[i];
    }
    for (unsigned int i = (hranica+1); i < (*populacia)[prg2].mcode.size(); i++) {
        newP1.mcode[i] = (*populacia)[prg2].mcode[i];
    }

    // vznik noveho chromozonu2
    for (int i = 0; i <= hranica; i++) {
        newP2.mcode[i] = (*populacia)[prg2].mcode[i];
    }
    for (unsigned int i = (hranica+1); i < (*populacia)[prg1].mcode.size(); i++) {
        newP2.mcode[i] = (*populacia)[prg1].mcode[i];
    }

    (*nova_populacia)[pos] = newP1;

    if (pos != nova_populacia->size()-1)
        (*nova_populacia)[pos++] = newP1;
}

#endif // GA_HH
