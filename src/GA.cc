#include "GA.hh"
#include "Program.hh"
#include "VMachine.hh"
#include "MyExceptions.hh"
#include <cstdlib>
#include <iostream>

using namespace std;

// ===============================================================================
// GA::GA(int populationSize, int epoches, VMachine *virtualMachine);
// ===============================================================================

/// Konstruktor

/// @li this->bestChromozon.fitnes = NEKONECNO
/// @li this->popSize = populationSize
///
/// @param populationSize velkost populacie
/// @note implicitne springsSize = 0.3 (pocet novych jedincov v populacii 30%), vytvorene konstruktorom
/// @throw MEX::myex_badparams
/// @throw MEX::myex_nullptr
GA::GA(int populationSize, VMachine *virtualMachine)
{
    if (populationSize < MIN_PSIZE) {
        cerr << "ERROR! GA::GA(): populationSize < " << MIN_PSIZE << endl;
        throw MEX::myex_badparams_object;
    }

    if (virtualMachine == NULL) {
        cerr << "ERROR! GA::GA(): virtualMachine = NULL" << endl;
        throw MEX::myex_nullptr_object;
    }

    this->vm = virtualMachine;

    this->t = 0;
    this->tMax = 0;

    this->popSize = populationSize;
    this->springsSize = 0.3;

    this->generationAvgFitnes = 0.0;

    // pre istotu - inak sa o to postara konstruktor triedy Program
    this->bestChromozon.fitnes = numeric_limits<double>::max();

    // imlicitna metodika evolucneho algoritmu
    this->evolution = &GA::newPopulationAlgorithm1;

    this->p1.clear();
    this->p2.clear();
}

// ===============================================================================
// GA::~GA()
// ===============================================================================

GA::~GA()
{
}


// ===============================================================================
// void GA::generateFirstPopulation();
// ===============================================================================

/// Vygenerovanie nultej generacie jedincov.

/// @li velkost programu = velkost pamete (pocet buniek) VMachine::getMemSize()
/// @li pocet generovanych jedincvo: GA::popSize;
///
/// @note Vysledok ulozi do this->p1
void GA::generateFirstPopulation()
{
    Program tmp;    // jeden generovany program

    unsigned int psize = 0;

    psize = this->vm->getMemSize();
    tmp.mcode.resize(psize);

    p1.clear();
    p2.clear();

    p1.resize(this->popSize);
    p2.resize(this->popSize);

    for (int i = 0; i < this->popSize; i++) {
        for (unsigned int j = 0; j < psize; j++) {
            if (this->vm->generate(&tmp.mcode[j]) == false) {
                cerr << "ERROR! GA::generateFirstPopulation(): this->vm->generat() return false" << endl;
            }
        }
        p1[i] = tmp;
    }
}


// ===============================================================================
// void GA::runEvolution(int generations)
// ===============================================================================

/// Geneticky algoritmus

/// @li Algoritmus vyuziva dve populacie : GA::p1, GA::p2. Jedna z populacii je aktualna populacia, druha je novo vytvorena z tej aktualnej. V kazdom behu sa pointre swapnu, z novej sa tak stane aktualne, a aktualna posluzi ako ulozisko kde sa budu ukladat nove jedince.
///
/// <table><tr><td>
/// <ul>
/// <li>generuj nultu populaciu jedincov</li>
/// <li>nulta generacia v GA::p1</li>
/// <li>for 0 ... GA::tMax</li>
/// <ul>
/// <li>vsetky jedince v populacii - vypocitaj fitnes, pridaj k priemeru fitnes generacie, zisti ci ide o najlepsieho jedinca</li>
/// <li>ukoncovacie kriterium, ak existuje</li>
/// <ul>
/// <li>napr.: fitnes1 - evolucia sa ukonci ihned ak su najdeme vsetky poklady</li>
/// <li>napr.: fitnes2 - ukoncovacie kriterium nema zmysel, nevieme ktora cesta je najkratsie, prebehne cela evolucia cez tMAX generacii</li>
/// </ul>
/// <li>vytvorenie novej populacie, metodika - registrovana funkcia:
/// <ul><li>(this->*evolution)(g, gNew);</li></ul>
/// </ul>
/// </ul>
/// </td></tr></table>
///
/// @param generations - pocet generacii (epoch)
/// @note ukoncovacie kriterium: zatial bez pouztia => evolucia cez vsetky epochy

void GA::runEvolution(int generations)
{
    vector<Program> *g = NULL;
    vector<Program> *gNew = NULL;
    vector<Program> *tmp = NULL;

    this->tMax = generations;

    this->t = 0;

    this->generateFirstPopulation();

    g = &this->p1;
    gNew = &this->p2;

    for (int i = 0; i < this->tMax; i++) {

        this->generationAvgFitnes = 0.0;

        // -------------------------------------------------------
        // ohodnotenie populacie + vypocet priemernej fitnes populacie
        // -------------------------------------------------------

        for (int j = 0; j < this->popSize; j++) {
            this->vm->loadProgram(&(*g)[j]);
            this->vm->runProgram();

            this->generationAvgFitnes += ( (*g)[j].fitnes / (double) this->popSize);
            this->checkAndSetBest(&(*g)[j]);

            //cout << "JEDINEC: " << j <<"; fitnes = " << (double) (*g)[j].fitnes << endl;
        }

        cout << "EPOCHA: " << i << "; BEST JEDINEC FITNES: " << (double) this->bestChromozon.fitnes << endl;
        cout << "EPOCHA: " << i << "; average generation fitnes: " << this->generationAvgFitnes << endl;

        // -------------------------------------------------------
        // bolo splnene ukoncovacie kriterium?
        // -------------------------------------------------------        

        /// @todo ukocovacie kriterium - iba ak fitnes1 - skonci akonahle su najdene vsetky poklady

        // -------------------------------------------------------
        // vytvorenie novej populacie
        // -------------------------------------------------------
        (this->*evolution)(g, gNew);

        // -------------------------------------------------------
        // swap g <--> gNew
        // -------------------------------------------------------
        tmp = g;
        g = gNew;
        gNew = tmp;
    }
}


// ===============================================================================
// void GA::newPopulationAlgorithm1(vector<Program> *p, vector<Program> *p_new)
// ===============================================================================

/// Metodika evolucie podla John R. Koza, Genetic Algorithms and Genetic Programming lectrure, Stanford University

/// John R. Koza - autor, "vynalezca" genetickeho programovania @n
/// http://www.genetic-programming.org/ @n
/// Inspiracia: http://www.genetic-programming.com/coursemainpage.html
/// @param *p
/// @param *p_new
/// @note predpoklad: p.len() == p_new.len()
///
void GA::newPopulationAlgorithm1(const vector<Program> * const p, vector<Program> * const p_new)
{
    int operation = 0;
    Program tmp;

     if (p->size() != p_new->size()) {
        cerr << "GA::newPopulationALgorithm1(): p.size != p_new.size" << endl;
        throw;
    }

    for (int i = 0; i < this->popSize; i++) {

//        cout << "CHROMOZON: " << i << ": ";

        operation = rand() % 3;

        // skopiruj do novej populacie
        if (operation == 0) {
//            cout << "KOPIROVANIE" << endl;
            (*p_new)[i] = (*p)[i];
        }

        // krizenie
        if (operation == 1) {
//            cout << "KRIZENIE" << endl;
            this->krizenie(p, p_new, i);
            // pretoze vzniknu dva nove jedince
            i = i + 1;
        }

        // mutacia
        if (operation == 2) {
//            cout << "MUTACIA" << endl;
            (*p_new)[i] = (*p)[i];
            this->mutation(&(*p_new)[i]);
        }        
    }
}
