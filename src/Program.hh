#ifndef PROGRAM_HH
#define PROGRAM_HH

#include "HW.h"
#include <vector>
#include <bitset>
#include <limits>

using namespace std;

/// Geneticky program.

/// Vykonatelny kod, fitnes hodnota.
///
class Program
{
public:
    vector<bitset<MAX_MEMCELL> > mcode;                   ///< strojovy kod programu, resp. obraz pameti programu, cely mcode sa pri vykonavani nahraje do pamete
    double fitnes;                                        ///< fitnes hotnota programu, ak bol ohodnoteny, inak nekonecno (najvacsia hodnota int)

public:
    Program();
};

#endif // PROGRAM_HH
