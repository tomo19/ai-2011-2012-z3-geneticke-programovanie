#include "GameWorld.hh"
#include "Position.hh"
#include <iostream>
#include <list>
#include <new>              // exception bad_alloc

using namespace std;

// tieto konstanty nemenit ani za behu programu!
// co, ake konstanty???

// =======================================================================================
//                                       PUBLIC
// =======================================================================================

// =======================================================================================
// GameWorld::GameWorld(int heigth, int width)
// =======================================================================================

GameWorld::GameWorld(int heigth, int width, bool cross)
{        
    this->startPos = NULL;
    this->actualPos = NULL;
    this->grid = NULL;

    this->heigth = 0;
    this->width = 0;

    this->findJewels = 0;
    this->jewels.clear();

    this->steps = 0;

    this->cross = cross;

    if ((heigth < MIN_HEIGTH) || (width < MIN_WIDTH) || (heigth > MAX_HEIGTH) || (width > MAX_WIDTH)) {
        cerr << "GameWorld::GameWorld(int, int): heigth=" << heigth << "; width=" << width << "; bad parameters!" << endl;
        throw;
    }

    this->heigth = heigth;
    this->width = width;

    this->jewelsHash.clear();

    // vytvorenie mriezky
    try {
        this->grid = new char*[heigth];

        for (int i = 0; i < heigth; i++)
            this->grid[i] = new char[width];
    } catch (bad_alloc &e) {
        cerr << "GameWorld::GameWorld(): " << e.what() << endl;
        throw e;
    }

    // inicializacia mriezky
    for (int i = 0; i < heigth; i++)
        for (int j = 0; j < width; j++)
            this->grid[i][j] = GW::SQ_EMPTY;

    // implicitna pozicia hladaca pokladov v strede mriezky + iniciakizacia startovacej pozicie
    try {
        this->startPos = new Position(((this->heigth-1)/2), ((this->width-1)/2));
        this->actualPos = new Position(((this->heigth-1)/2), ((this->width-1)/2));
        this->gridPos = new Position(((this->heigth-1)/2), ((this->width-1)/2));
    } catch (bad_alloc &e) {
        cerr << "GameWorld::GameWorld(): " << e.what() << endl;
        throw e;
    }

    // zobrazenie hladaca (implicitna pozicia) do mriezky
    this->grid[this->startPos->x][this->startPos->y] = GW::SQ_FINDER;
}

// =======================================================================================
// GameWorld::~GameWorld()
// =======================================================================================

GameWorld::~GameWorld()
{
    // uvolnenie mriezky z pamete
    if (this->grid != NULL) {
        for (int i = 0; i < heigth; i++) {
            if (this->grid[i] != NULL) {
                delete [] this->grid[i];
                this->grid[i] = NULL;
            }
        }


        delete [] this->grid;
        this->grid = NULL;
    }

    if (this->startPos != NULL) {
        delete this->startPos;
        this->startPos = NULL;
    }

    if (this->actualPos != NULL) {
        delete this->actualPos;
        this->actualPos = NULL;
    }

    if (this->gridPos != NULL) {
        delete this->gridPos;
        this->gridPos = NULL;
    }
}

// =======================================================================================
// bool GameWorld::setStartPos(Position *pos)
// =======================================================================================

/// @warning zaroven nastavy aj aktualnu poziciu hladacia pokladov na novu startovaciu poziciu
/// @warning pouzivat vylucne sposobom GameWorld::setStartPos(new Position(3,2));
/// @warning metoda uvolni z pamete *pos
/// @note vyhodi prazdnu vynimku, do buducna vytvorit vlastny typy vynimiek
bool GameWorld::setStartPos(const Position *const pos)
{    
    // ak nejaky z clenov nieje vytvoreny - vyhodime vynimku

    if (this->grid == NULL) {
        cerr << "ERROR! GameWorld::setStartPos(): this->grid = NULL !" << endl;
        delete pos;
        throw;
    }

    if (pos == NULL) {
        cerr << "ERROR! GameWorld::setStartPos(): pos = NULL !" << endl;
        delete pos;
        throw;
    }

    if (this->startPos == NULL) {
        cerr << "ERROR! GameWorld::setStartPos(): this->startPos = NULL !" << endl;
        delete pos;
        throw;
    }

    if (this->actualPos == NULL) {
        cerr << "ERROR! GameWorld::setStartPos(): this->actualPos = NULL !" << endl;
        delete pos;
        throw;
    }

    // kontrola novej pozicie - suradnice fit in grid
    if (GameWorld::checkRange(pos->x, pos->y) == false) {
        cerr << "ERROR! GameWorld::setStartPos(): *pos out of range!" << endl;
        delete pos;
        throw; /// \todo iny sposob osetrenie ak ma algoritmus pokracovat aj ked je v omyle
    }

    // prarameter = ta ista startovacia suradnica - koniec
    if ((pos->x == this->startPos->x) && (pos->y == this->startPos->y)) {
        cerr << "WARNING! GameWorld::setStartPos(): same position" << endl;
        delete pos;
        return true;
    }

    // nastav novu startovaciu poziciu
    *(this->startPos) = *pos;

    // resetuj aktualnu poziciu na start
    *(this->actualPos) = *pos;

    // zobraz na mriezke
    // startovaciu poziciu musime premietnut do mriezky - aby sme zaistili spravne fungovanie GameWorld::addJewel()
    updateGridPos();

    // zisti, ci sa nenachadzas na poklade
    if (isJewel() == true) {
        if (this->jewelsHash.contains(pos->hash()) == false) {
            this->jewelsHash.insert(pos->hash(), *pos);
            this->findJewels++;
        }
    }

    delete pos;
    return true;
}

// =======================================================================================
// bool GameWorld::addJewel(Position *pos)
// =======================================================================================

/// @warning \li volat iba pre prvym volanim metody GameWorld::move(), ak sa zavola po volani tejto metody - neprevidatelne spravanie (dovod: po zmene startovacej pozicie mame zarucene, ze ta bude premietnuta do mriezky, odtial GameWorld::move() pri ukladani pokladov vie zistit ci sa na danom policku nachadza aj finder, pohyby po mriezke niesu synchronizovane s grafickym vystupom)
/// @warning \li pouzivat vylucne sposobom GameWorld::addJewel(new Position(3,2));
/// @warning \li metoda uvolni z pamete *pos
bool GameWorld::addJewel(const Position * const pos)
{
    if (pos == NULL) {
        cerr << "ERROR! GameWorld::addJewel(): pos = NULL;" << endl;
        delete pos;
        throw;
    }

    if (this->grid == NULL) {
        cerr << "ERROR! GameWorld::addJewel(): this->grid = NULL !" << endl;
        delete pos;
        throw;
    }

    // kontrola: parameter - fit in mriezka
    if (checkRange(pos->x, pos->y) == false) {
        cerr << "ERROR! GameWorld::addJewel(): *pos out of range!" << endl;
        delete pos;
        throw; /// \todo iny sposob osetrenie ak ma algoritmus pokracovat aj ked je v omyle
    }

    // ak tu poklad uz je umiesteny - koniec
    if ((this->grid[pos->x][pos->y] == GW::SQ_JEWEL) || (this->grid[pos->x][pos->y] == GW::SQ_FONJ)) {
        cerr << "WARNING: GameWorld::addJewel(): same position" << endl;
        delete pos;
        return true;
    }

    /// \todo spravit clanok s prikladom: tu bolo predtym else, a dva if nahor po delete pos nasledoval return, return sa zmazal -> ine vnaranie
    if ((this->grid[pos->x][pos->y] != GW::SQ_EMPTY) && (this->grid[pos->x][pos->y] != GW::SQ_FINDER)) {
        cerr << "WARNING! GameWorld::addJewel(): this square is not empty! (other obbject than finder or jewel)" << endl;
        delete pos;
        return false;
    }
    // ak prazne policka - umiestni (zobraz) poklad
    if (this->grid[pos->x][pos->y] == GW::SQ_EMPTY) {
        this->jewels.push_back(*pos);
        this->grid[pos->x][pos->y] = GW::SQ_JEWEL;
    }
    // ak hladac na policku - zobraz hladaca aj poklad
    if (this->grid[pos->x][pos->y] == GW::SQ_FINDER) {
        this->jewels.push_back(*pos);
        this->grid[pos->x][pos->y] = GW::SQ_FONJ;

        // zaroven novy najdeny poklad
        if (this->jewelsHash.contains(pos->hash()) == false) {
            this->jewelsHash.insert(pos->hash(), *pos);
            this->findJewels++;
        }
    }

    delete pos;
    return false;
}

// =======================================================================================
// bool GameWorld::move()
// =======================================================================================

bool GameWorld::move(GW::e_move direction)
{
    // pre kazdy pohyb:
    // - ak hranica
    //   - ak cross == true -> posun; steps++; return TRUE
    //   - ak cross == false -> return FALSE
    // - else
    //   - steps++
    //   - posun
    // - ak aktualna pozicia na poklad -> findJewels++

    switch (direction) {

    // -------------------------------------
    // NAHOR
    // -------------------------------------
    case GW::MOV_H:
        // ak hranicna podmienka
        if (this->actualPos->x == 0) {
            if (this->cross == false)
                return false;
            if (this->cross == true) {
                this->actualPos->x = this->heigth - 1;
                this->steps = this->steps + 1;
                break;
            }
        }

        // pohyb
        this->actualPos->x = this->actualPos->x - 1;
        this->steps = this->steps + 1;
        break;

        // -------------------------------------
        // NADOL
        // -------------------------------------
    case GW::MOV_D:
        // ak hranicna podmienka
        if (this->actualPos->x == this->heigth-1) {
            if (this->cross == false)
                return false;
            if (this->cross == true) {
                this->actualPos->x = 0;
                this->steps = this->steps + 1;
                break;
            }
        }

        // pohyb
        this->actualPos->x = this->actualPos->x + 1;
        this->steps = this->steps + 1;
        break;


        // -------------------------------------
        // VLAVO
        // -------------------------------------
    case GW::MOV_L:
        // ak hranicna podmienka
        if (this->actualPos->y == 0) {
            if (this->cross == false)
                return false;
            if (this->cross == true) {
                this->actualPos->y = this->width-1;
                this->steps = this->steps + 1;
                break;
            }
        }

        // pohyb
        this->actualPos->y = this->actualPos->y - 1;
        this->steps = this->steps + 1;
        break;

        // -------------------------------------
        // VPRAVO
        // -------------------------------------
    case GW::MOV_P:
        // ak hranicna podmienka
        if (this->actualPos->y == this->width-1) {
            if (this->cross == false)
                return false;
            if (this->cross == true) {
                this->actualPos->y = 0;
                this->steps = this->steps + 1;
                break;
            }
        }

        // pohyb
        this->actualPos->y = this->actualPos->y + 1;
        this->steps = this->steps + 1;
        break;

    default:
        cerr << "ERROR! GameWorld::move() bad direction!" << endl;
        return false;               /// @note pouzvazovat nad throw
    }

    if (isJewel() == true) {
        if (this->jewelsHash.contains(actualPos->hash()) == false) {
            this->jewelsHash.insert(actualPos->hash(), *actualPos);
            this->findJewels++;
        }
    }

    return false;
}

// =======================================================================================
// void GameWorld::restart()
// =======================================================================================

void GameWorld::restart()
{
    if (this->grid == NULL) {
        cerr << "ERROR! GameWorld::restart(): this->grid = NULL" << endl;
        throw;
    }

    if (this->startPos == NULL) {
        cerr << "ERROR! GameWorld::restart(): this->startPos = NULL" << endl;
        throw;
    }

    if (this->actualPos == NULL) {
        cerr << "ERROR! GameWorld::setStartPos(): this->actualPos = NULL !" << endl;
        throw;
    }

    // reset najdenych pokladov a poctu vykonanych krokov
    this->findJewels = 0;
    this->steps = 0;
    this->jewelsHash.clear();

    // vyscisti mriezku
    for (int i = 0; i < this->heigth; i++) {
        for (int j = 0; j < this->width; j++) {
            this->grid[i][j] = GW::SQ_EMPTY;
        }
    }

    // startovacia pozicia = {aktualna pozicia, aktualna pozicia na mriezke}
    *(this->actualPos) = *(this->startPos);

    this->updateGridPos();

    // nahod spat poklady zo zoznamu pokladov
    list<Position>::iterator it = this->jewels.begin();
    while (it != this->jewels.end()) {
        // ak na policku uz je poklad
        if ((this->grid[(*it).x][(*it).y] == GW::SQ_JEWEL) || (this->grid[(*it).x][(*it).y] == GW::SQ_FONJ)) {
            cerr << "WARNING: GameWorld::restart(): jewel on the same position" << endl;
            continue;
        }
        // ak je policko prazdne
        if (this->grid[(*it).x][(*it).y] == GW::SQ_EMPTY) {
            this->grid[(*it).x][(*it).y] = GW::SQ_JEWEL;
        }
        // ak je na policku hladac
        if (this->grid[(*it).x][(*it).y] == GW::SQ_FINDER) {
            this->grid[(*it).x][(*it).y] = GW::SQ_FONJ;
        }

        it++;
    }

    if (isJewel() == true) {
        if (this->jewelsHash.contains(actualPos->hash()) == false) {
            this->jewelsHash.insert(actualPos->hash(), *actualPos);
            this->findJewels++;
        }
    }

}

// =======================================================================================
// void GameWorld::clear()
// =======================================================================================

void GameWorld::clear()
{
    if (this->grid == NULL) {
        cerr << "WARNING! GameWorld::clear(): this->grid = NULL" << endl;
        return;
    }

    if (this->startPos == NULL) {
        cerr << "WARNING! GameWorld::clear(): this->startPos = NULL" << endl;
        return;
    }

    if (this->actualPos == NULL) {
        cerr << "WARNING! GameWorld::clear(): this->actualPos = NULL" << endl;
        return;
    }

    if (this->gridPos == NULL) {
        cerr << "WARNING! GameWorld::clear(): this->gridPos = NULL" << endl;
        return;
    }

    // vycisti mriezky
    for (int i = 0; i < this->heigth; i++) {
        for (int j = 0; j < this->width; j++) {
            this->grid[i][j] = GW::SQ_EMPTY;
        }
    }

    // znova nastav startovaciu poziciu
    this->startPos->x = (this->heigth-1)/2;
    this->startPos->y = (this->width-1)/2;
    *(this->actualPos) = *(this->startPos);

    updateGridPos();

    // reset najdenych pokladov a poctu vykonanych krokov
    this->findJewels = 0;
    this->steps = 0;

    // zmaz zoznam policok s pokladmi
    this->jewels.clear();
    this->jewelsHash.clear();
}

// =======================================================================================
// void GameWorld::toString() const
// =======================================================================================

void GameWorld::toString()
{
    if (this->grid == NULL) {
        cerr << "ERROR: GameWorld::toString(): this->grid = NULL" << endl;
        return;
    }

    updateGridPos();

    // horizontalna hlavicka
    // prva horizontalna ciara
    cout << "-----";
    for (int i = 0; i < this->width; i++)
        cout << "---";
    cout << endl;
    // text hlavicky
    cout << "   | ";
    for (int i = 0; i < this->width; i++) {
        cout.width(2);
        cout << i << " ";
    }
    cout << endl;
    // druha horizontalna ciara
    cout << "-----";
    for (int i = 0; i < this->width; i++)
        cout << "---";
    cout << endl;

    // hlavny vypis
    for (int i = 0; i < this->heigth; i++) {
        cout.width(2);
        cout << i << " | ";
        for (int j = 0; j < this->width; j++) {
            cout.width(2);
            cout << this->grid[i][j] << " ";
        }
        cout << endl;
    }

    // ciara na konci
    cout << "-----";
    for (int j = 0; j < this->width; j++)
        cout << "---";
    cout << endl;
}

// =======================================================================================
//                                       PRIVATE
// =======================================================================================

// =======================================================================================
// bool GameWorld::isJewel()
// =======================================================================================

/// Poklad na aktualnej pozicii

bool GameWorld::isJewel() const
{
    if ((this->grid[this->actualPos->x][this->actualPos->y] == GW::SQ_JEWEL) || (this->grid[this->actualPos->x][this->actualPos->y] == GW::SQ_FONJ))
        return true;

    return false;
}

// =======================================================================================
// bool GameWorld::checkRange(int x, int y) const
// =======================================================================================

bool GameWorld::checkRange(int x, int y) const
{
    if ((x < 0) || (y < 0) || (x > (this->heigth-1)) || (y > (this->width-1)))
        return false;

    return true;
}

// =======================================================================================
// void GameWorld::updateActPos()
// =======================================================================================

/// Premietne aktualnu poziciu do mriezky

/// Uzitocne iba na zobrazenie cesty hladaca pri ladeni.
/// @note Podstatne je, ze s pokladmi na mriezke by sa pocas
/// behu programu nemalo pohybovat, a vieme teda z this->actualPos zistit, ci sa nachadzame na policku s pokladom
/// aj bez toho, aby sme hladaca v kazdom kroku vykreslovali do mriezky.

void GameWorld::updateGridPos()
{
    // ak sa nehybeme
    if ((this->grid[this->actualPos->x][this->actualPos->y] == GW::SQ_FINDER) || (this->grid[this->actualPos->x][this->actualPos->y] == GW::SQ_FONJ))
        return;

    delActPos();

    // ak je policko prazdne
    if (this->grid[this->actualPos->x][this->actualPos->y] == GW::SQ_EMPTY)
        this->grid[this->actualPos->x][this->actualPos->y] = GW::SQ_FINDER;

    if (this->grid[this->actualPos->x][this->actualPos->y] == GW::SQ_JEWEL)
        this->grid[this->actualPos->x][this->actualPos->y] = GW::SQ_FONJ;

    *(this->gridPos) = *(this->actualPos);
}

// =======================================================================================
// void GameWorld::delActPos()
// =======================================================================================

/// Zmaze hladaca na grafickom vystupe mriezky z pozicie, kde sa nachadzal na posledy

/// \note Pozicia kde sa nachadzal hladac na mriezke naposledy(v grafickom vystup) nemusi byt totozna s aktualnou poziciou hladaca na mriezke.
/// Viac info v metoda GameWorld::updateActPos()
/// \warning jendnoducha, rychla funkcia, nekontroluje sa this->grid != NULL

void GameWorld::delActPos()
{
    if (this->grid[this->gridPos->x][this->gridPos->y] == GW::SQ_FONJ) {
        this->grid[this->gridPos->x][this->gridPos->y] = GW::SQ_JEWEL;
        return;
    }

    this->grid[this->gridPos->x][this->gridPos->y] = GW::SQ_EMPTY;
}
